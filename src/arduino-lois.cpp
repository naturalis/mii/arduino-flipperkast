// Created for: Naturalis Biodiversity Center
// Created by: Frank Vermeij & Titus Kretzschmar
// License: CC BY-SA 4.0

#include <Keyboard.h>

#define Inp_0 A0
#define Inp_1 A1
#define Inp_2 A2
#define PRESS_DELAY 50

#define Led 13

int knop0 = HIGH;
int knop1 = HIGH;
int knop2 = HIGH;

void setup() {
  pinMode(Inp_0,INPUT_PULLUP);
  pinMode(Inp_1,INPUT_PULLUP);
  pinMode(Inp_2,INPUT_PULLUP);

  pinMode(Led,OUTPUT);
  digitalWrite(Led,LOW); // Led uit

  Keyboard.begin();
}

void CheckInputs(void)
{
  knop0 = digitalRead(Inp_0);
  knop1 = digitalRead(Inp_1);
  knop2 = digitalRead(Inp_2);

  if(knop0 == LOW)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(0x20);  // send '0x20' which is a space
  } else {
    Keyboard.release(0x20);
    digitalWrite(Led,LOW); // Led uit
  }

  if(knop1 == LOW)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY_RIGHT_SHIFT);  // send 'KEY_RIGHT_SHIFT'
  } else {
    Keyboard.release(KEY_RIGHT_SHIFT);
    digitalWrite(Led,LOW); // Led uit
  }

  if(knop2 == LOW)
  {
    digitalWrite(Led,HIGH); // Led aan
    Keyboard.press(KEY_LEFT_SHIFT);  // send 'KEY_LEFT_SHIFT'
  } else {
    Keyboard.release(KEY_LEFT_SHIFT);
    digitalWrite(Led,LOW); // Led uit
  }

}

void loop() {
  CheckInputs();
  delay(PRESS_DELAY);
}
